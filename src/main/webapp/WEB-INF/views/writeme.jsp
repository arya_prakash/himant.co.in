<jsp:include page="header.jsp" />
<jsp:include page="sideNavBar.jsp"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
	<h1>Write About me..</h1>
	<div class="container">
<div class="panel panel-default ">
                <!-- Default panel contents -->
              <!-- <div class="panel-heading">Messages</div> -->
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Sender</th>
                              <th>Message</th>
                          </tr>
                      </thead>
                      <tbody>
                      <c:forEach items="${messages}" var="message"> 
                             <tr> 
                                 <td>${message.sender}</td> 
                                 <td>${message.messageText}</td> 
                             </tr> 
                         </c:forEach> 
                      </tbody>
                  </table>
          </div>
    </div>
</main> 



<jsp:include page="footer.jsp" />