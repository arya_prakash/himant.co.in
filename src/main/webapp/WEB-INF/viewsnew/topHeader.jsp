<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
function onSignIn(googleUser) {
	  var profile = googleUser.getBasicProfile();
	  var idToken = googleUser.getAuthResponse().id_token;
	  data = 'tokenString='+idToken;
	  alert(idToken);
	  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	  console.log('Name: ' + profile.getName());
	  console.log('Image URL: ' + profile.getImageUrl());
	  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	  
	// Send the code to the server
	    $.ajax({
	      type: 'POST',
	      url: 'http://localhost:8082/Mysite/login',
	      // Always include an `X-Requested-With` header in every AJAX request,
	      // to protect against CSRF attacks.
	      /* headers: {
	        'X-Requested-With': 'XMLHttpRequest'
	      }, */
	      //contentType: 'application/octet-stream; charset=utf-8',
	      success: function(result) {
	        console.log('successfully sent data to controller')
	        $("#profile").show();
	        $("#profileImg").attr("src", profile.getImageUrl());
	        
	      },
	      processData: false,
	      data: data
	    });
	  
	  
	}
	
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
      $("#profile").hide();
    });
  }
  
$(document).ready(function(){
    $("#profile").hide();
    
    });
    
</script>

<a href="#" onclick="signOut();">Sign out</a>


<div class="top-nav wow">
	<div class="container">
		<div class="navbar-header logo">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				Menu</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse fixed-top"
			id="bs-example-navbar-collapse-1">
			<div class="menu navbar-left" id="topMenu">

				<ul class="nav navbar navbar-left">
					<c:choose>
						<c:when test="${activeMenu == 'index'}">
							<li><a href="/Mysite/" class="scroll active">Home</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/Mysite/" class="scroll">Home</a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${activeMenu == 'aboutMe'}">
							<li><a href="/Mysite/aboutMe" class="scroll active">About</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/Mysite/aboutMe" class="scroll">About</a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${activeMenu == 'imageGallery'}">
							<li><a href="/Mysite/imageGallery" class="scroll active">Image Gallery</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/Mysite/imageGallery" class="scroll">Image Gallery</a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${activeMenu == 'videoGallery'}">
							<li><a href="/Mysite/videoGallery" class="scroll active">Video Galllery</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/Mysite/videoGallery" class="scroll">Video Galllery</a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${activeMenu == 'contact'}">
							<li><a href="/Mysite/contact" class="scroll active">Contact</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/Mysite/contact" class="scroll">Contact</a></li>
						</c:otherwise>
					</c:choose>
					<li><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success">Sign
							in</button></li>
					<li><button type="button" class="btn btn-info">Sign
							up</button></li>
							
					
						<div id="profile">
						<div class="col-md-3 col-sm-3 col-xs-4 profile-pic"><img id="profileImg" class="img-thumbnail"></div>
						<div class="col-md-9 col-sm-9 col-xs-8 profile-about">
							<h2>WEBi</h2>
							<p><i class="glyphicons glyphicons-riflescope"></i> <a href="#">Texxmedia</a></p>
						</div>
						</div>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
          <h4 class="modal-title">Log-in</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
    		<label for="exampleInputEmail1">Email address</label>
    		<input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email">
  		  </div>
		  <div class="form-group">
		  	<label for="exampleInputPassword1">Password</label>
			<input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
		  </div>
          <p class="text-right"><a href="#">Forgot password?</a></p>
        </div>
        <div class="g-signin2" data-onsuccess="onSignIn"></div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Close</a>
          <a href="#" class="btn btn-primary">Log-in</a>
        </div>
      </div>
    </div>
</div>