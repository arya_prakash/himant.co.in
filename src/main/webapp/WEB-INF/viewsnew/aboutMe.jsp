<jsp:include page="header.jsp" />
<jsp:include page="topHeader.jsp" />
<div id="about" class="about">
	<div class="container">
		<h3 class="title">About Me</h3>
		<div class="col-md-8 about-left">
			<p>Hello Friend!!!
				My name is Himant Dewangan and I am currently working as software developer with 
				coMakeIT software private Ltd for last 2 years.
				Suspendisse laoreet sem sit amet dolor luctus pellentesque.
				Pellentesque eleifend tellus at interdum elementum. Nam egestas
				molestie elit. Vivamus sed accumsan quam, a mollis magna. Nam
				aliquet eros eget sapien consequat tincidunt at vel nibh. Duis ut
				turpis mi. Duis nec scelerisque urna, sit amet varius arcu. Aliquam
				aliquet sapien quis mauris semper suscipit. Maecenas pharetra
				dapibus posuere. Praesent odio sem, varius quis dolor vel, maximus
				dapibus mi. Pellentesque mattis mauris neque. Nam aliquam turpis
				ante, at cursus massa ullamcorper ut. Proin id diam id nisi sagittis
				pellentesque sed sit amet eros. In porttitor tempus nulla, a porta
				purus commodo sed. Praesent hendrerit nisi nunc, ut porttitor justo
				pellentesque et ac gravida sem mattis. Donec ornare justo nec</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Suspendisse laoreet sem sit amet dolor luctus pellentesque Nam
				egestas molestie elit. Vivamus sed accumsan quam, a mollis magna.
				Nam aliquet eros eget sapien consequat</p>
		</div>
		<div class="col-md-4 about-right">
			<ul>
				<h5>Awards</h5>
				<li><span class="glyphicon glyphicon-menu-right"></span> Lorem
					ipsum dolor sit amet cingelit</li>
				<li><span class="glyphicon glyphicon-menu-right"></span>
					Curabitur id metus rutrum convallis</li>
				<li><span class="glyphicon glyphicon-menu-right"></span> Morbi
					dictum velit vitae porttitor</li>
				<li><span class="glyphicon glyphicon-menu-right"></span> Fusce
					at metus id justo ullamcorper</li>
				<li><span class="glyphicon glyphicon-menu-right"></span>
					Aliquam ac nisl id justo malesuada</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="education" class="education">
		<div class="container">
			<h3 class="title">Education</h3>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Master Of Computer Application - 2011</h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-education"> </span> NIT Raipur</h5>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo </p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Bachelor Of Computer Application - 2010</h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> Pt. RaviShankar Shukla University Raipur <span class="glyphicon glyphicon-education"></span></h5>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo </p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Web Design - 2008</h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-education"> </span> University Name</h5>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo </p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>