<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>Himant Dewangan</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
     <meta name="google-signin-client_id" content="853022612096-r0r32ptelo5nlajnbgq2b8f8bf1aevfv.apps.googleusercontent.com">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="icon" href="staticnew/img/photo.jpg" type="image" sizes="16x16">
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
  	<link rel="stylesheet" href="staticnew/css/bootstrap.css" /> 
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="staticnew/js/bootstrap.min.js"></script>
    <script src="staticnew/js/main.js"></script>
    <script src="staticnew/js/bootstrap-imageupload.js"></script>
    <link rel="stylesheet" href="staticnew/css/bootstrap-imageupload.css" />
    <!--END GLOBAL STYLES -->
    
    <!-- Google sign in required JS -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!-- end here Google sign in -->

    <!-- PAGE LEVEL STYLES -->
    <link href="staticnew/css/jquery.bsPhotoGallery.css" rel="stylesheet">
     <script src="staticnew/js/jquery.bsPhotoGallery.js"></script>
    
    
    <link href='//fonts.googleapis.com/css?family=Overlock:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="staticnew/css/style.css" /> 
    <!-- <link rel="stylesheet" href="staticnew/css/bootstrap-imageupload.css" /> -->
    
    <!--  FIle Upload plugin -->
	<link href="staticnew/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
     This must be loaded before fileinput.min.js -->
	<!-- <script src="staticnew/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script> -->
	<!-- the main fileinput plugin file -->
	<script src="staticnew/js/fileinput.min.js"></script>
	<!-- optionally if you need a theme like font awesome theme you can include 
    it as mentioned below -->
	<!-- <script src="staticnew/js/theme.js"></script> -->
	<!-- optionally if you need translation for your language then include 
    locale file as mentioned below -->
	<!-- <script src="staticnew/js/locales/.js"></script> -->
    
    
</head>