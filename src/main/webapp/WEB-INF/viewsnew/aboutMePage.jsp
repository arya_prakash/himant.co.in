<jsp:include page="header.jsp" />
<jsp:include page="topHeader.jsp" />
<div id="about" class="about">
	<div class="container">
		<h3 class="title">About Me</h3>
		<div class="col-md-8 about-left">
			<p>Hello Friends !!!!<br/>
			I am <strong>Himant Dewangan</strong> a full stack Java Developer.
			I write interactive programs to solve business needs. 
			I am currently working with coMakeIT software private Ltd, Hyderabad. 
			</p>
			<br/>
			<p>I am very much interested in learning new things in technology. I built this web-site just to have good hands-on on <strong>Bootstarp</strong>.
			I tried to cover various rich featured User Interface components in this web-sites provided by Bootstrap. At the back end of the site, there is <strong>Spring MVC</strong>
			</p>
			<br/>
			<p>
			My other areas of Interests are listening music and playing guitar. I am big fond of music, I listen various kinds of music which includes our bollywood songs, English songs,
			some time I also listen Telugu, bengali songs as well. Actually Every song that has good music and beautiful guitar backing track I love to listen. 
			Some of our recording in which I am covering my vocal lead on guitar can be found in our Video Garllery section. Hope you enjoy them listening. 
			</p>
			
			<br/>
			<p>
				My Education and my work experience details can be found in below section. Please go through it to know more.
			</p>
			
		</div>
		<div class="col-md-4 about-right">
			<ul>
				<h5>Awards</h5>
				<li><span class="glyphicon glyphicon-menu-right"></span> OCJP 1.6 certified Java Professional</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="education" class="education">
		<div class="container">
			<h3 class="title">Education</h3>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Master Of Computer Application - 2011</h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-education"> </span> NIT Raipur</h5>
					<p>After qualifying in NIMCET 2008, I took amission in National Institute of Technology Raipur to persue Master of Computer Application.
					I completed my Post Graduation in 2011 with 8.00 CGPI</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Bachelor of Computer Application - 2008</h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> Pt. Ravishankar Shukla University Raipur <span class="glyphicon glyphicon-education"></span></h5>
					<p>I completed my Grauation in Computer Application stream in 2008 securing 73%.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Higher Secondary  - 2004</h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-education"> </span> Chhattisgarh State Board</h5>
					<p>I completed my Higher secondary school certificate examination in 2004 having the subjects Physics, Chemistry, Math and Biology securing first devision.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div id="work" class="education">
		<div class="container">
			<h3 class="title">Work Experience</h3>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>May 2015 - Present </h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> ComakeIT <span class="glyphicon glyphicon-briefcase"> </span></h5>
					<p>I joined coMakeIT software pvt ltd on May 2015 as Software engineer and it is my current Employer. Its product based company who is providing product for Life Insurance and pension solution product for our customer based in Netherland.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>May 2014 - May 2015 </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-briefcase"> </span> Oracle</h5>
					<p>After having great experience in Sapient I moved to Oracle India pvt Ltd, and worked there for one year as Application Engineer for Fusion application.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Jan 2011 - May 2014 </h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> Sapient Consulting Pvt Ltd.<span class="glyphicon glyphicon-briefcase"></span> </h5>
					<p>I joined Sapient in Jan 2011 as Associate Technology L1, And worked with leading banking client UBS as java developer. </p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"/>