<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="header.jsp" />
<body class="padTop53 ">
	<!-- MAIN WRAPPER -->
	<div id="wrap">
		<jsp:include page="topHeader.jsp" />
		<!-- Menu bar section -->
			<div id="left">
				<div class="media user-media well-small">
					<a class="user-link" href="#"> <img
						class="media-object img-thumbnail user-img" alt="User Picture"
						src="staticnew/img/user.jpg">
					</a> <br>
					<div class="media-body">
						<h5 class="media-heading">Himant Dewangan</h5>
					</div>
					<br>
				</div>
	
				<ul id="menu" class="collapse">
					<li class="panel"><a href="/Mysite/"> About Me</a></li>
					<li class="panel "><a href="/Mysite/videoGallery"> Video
							Gallery </a></li>
					<li class="panel "><a href="imageGallery.jsp"> Image
							Gallery </a></li>
					<li class="panel active"><a href="/Mysite/writeme"> Write Me </a></li>
				</ul>
			</div>

		<!--  End of menu bar section -->
		<jsp:include page="writeMeContent.jsp" />

	</div>
	<!--  END OF MAIN WRAPPER -->

	<jsp:include page="footer.jsp" />

</body>
<!-- END BODY -->

</html>