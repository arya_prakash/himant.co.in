<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<button type="button" class="btn btn-primary" data-toggle="modal"
	data-target="#exampleModal" data-whatever="@mdo">New Album</button>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="exampleModalLabel">New Album</h4>
			</div>
			<div id="successAlert" class="alert alert-success">
				<strong>Success!</strong> New Album created.
			</div>
			<div class="modal-body">
			<!-- commandName="albumVO" action = "/Mysite/createAlbum" enctype="multipart/form-data" method="POST" -->
				<form:form id="albumForm" commandName="albumVO"  >
					<div class="form-group">
						<label for="recipient-name" class="control-label">Title:</label> 
						<input name="title" type="text" class="form-control" id="recipient-name">
					</div>
					<div class="form-group">
						<label for="message-text" class="control-label">Description:</label>
						<textarea name="description" class="form-control" id="message-text"></textarea>
					</div>
						<div class="imageupload panel panel-default">
							<div class="panel-heading clearfix">
								<h3 class="panel-title pull-left">Upload Cover Page</h3>
							</div>
							<div class="file-tab panel-body">
								<label class="btn btn-primary btn-file"> <span>Browse</span>
									<input name="coverPage" type="file" >
									<!-- name="image-file" -->
								</label>
								<button type="button" class="btn btn-danger">Remove</button>
							</div>
						</div>
					</form:form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success" id="submitButton">Save</button>
			</div>
		</div>
	</div>
</div>

<!--  Need to intialize the file upload -->
<script>
	var $imageupload = $('.imageupload');
	$imageupload.imageupload();
	
	
	jQuery(document).ready(function($) {
		$("#successAlert").hide();

		$("#submitButton").click(function(event) {

			// Prevent the form from submitting via the browser.
			event.preventDefault();

			searchViaAjax();

		});

	});

	function searchViaAjax() {


		var form = $("#albumForm")[0];
	    var data = new FormData(form);
	    
		$.ajax({
			type : "POST",
			enctype: 'multipart/form-data',
			data: data,
			contentType: false,
			url : "/Mysite/createAlbum",
			processData: false,
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				$("#successAlert").show();
				$("#albumForm")[0].reset();
				$("#coverPage").val('');
				window.location.reload();
			},
		});

	}
	
</script>