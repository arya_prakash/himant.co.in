<jsp:include page="header.jsp" />
<jsp:include page="topHeader.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">
.albumCover {
	background-size: cover !important;
	/* height: 300px !important; */
	width: 220px !important;
}

.albumCoverImg {
	object-fit: cover !important;
	width: 100% !important;
	height: 220px;
}
</style>


<div id="about" class="about">
	<div class="container">
		<h3 class="title">Photo Album</h3>
		<div class="row">
			<c:forEach items="${albums}" var="album">
				<div class="col-md-3">
					<div class="well albumCover ">
						<form action="/Mysite/gotoAlbum" id="myForm${album.albumID}"
							method="POST">
							<input type="hidden" name="albumId" value="${album.albumID}" />
							<a href='#'
								onclick="document.getElementById('myForm${album.albumID}').submit();"><img
								class="thumbnail img-responsive albumCoverImg " width="250px"
								height="250px" alt="Bootstrap template"
								src="${album.coverPageUrl}" /></a>
						</form>
						<div class="caption">
							<p>${album.title}</p>
							<span class="glyphicon glyphicon-thumbs-up"></span>
							<!-- <a href="#">
								<span class="glyphicon glyphicon-trash"></span>
							</a> -->

						</div>
						<div class="file-actions">
							<div class="file-footer-buttons">
								<button type="button"
									class="kv-file-upload btn btn-xs btn-default"
									title="Upload file">
									<i class="glyphicon glyphicon-upload text-info"></i>
								</button>
								<button type="button"
									class="kv-file-remove btn btn-xs btn-default"
									title="Remove file">
									<i class="glyphicon glyphicon-trash text-danger"></i>
								</button>
								<button type="button"
									class="kv-file-zoom btn btn-xs btn-default"
									title="View Details">
									<i class="glyphicon glyphicon-zoom-in"></i>
								</button>
							</div>
							<!-- <div class="clearfix"></div> -->
						</div>
						
					</div>
				</div>
			</c:forEach>
		</div>

		<jsp:include page="newAlbumModal.jsp" />
	</div>
</div>
