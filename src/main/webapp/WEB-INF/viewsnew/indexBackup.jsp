<jsp:include page="header.jsp" />

	<body class="padTop53 " >
	    <!-- MAIN WRAPPER -->
	    <div id="wrap" >
	    	<jsp:include page="topHeader.jsp" />
	    	<jsp:include page="sideMenuBar.jsp" />
	    	<jsp:include page="aboutMePage.jsp" />
	    	
	    </div>
	    
	    <!--  END OF MAIN WRAPPER -->
	    
	    <jsp:include page="footer.jsp" />
	    
	</body>
	<!-- END BODY -->
	
</html>