<jsp:include page="header.jsp" />
<jsp:include page="topHeader.jsp"/>

<div id="home" class="banner">
		<div class="banner-info">
			<div class="container">
				<div class="col-md-4 header-left">
					<img src="staticnew/img/mypic2.JPG" alt="">
				</div>
				<div class="col-md-8 header-right">
					<h2>Hello</h2>
					<h1>I'm Himant Dewangan</h1>
					<h6>Software Developer</h6>
					<ul class="address">
						<li>
							<ul class="address-text">
								<li><b>D.O.B</b></li>
								<li>28-03-1988</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>PHONE </b></li>
								<li>+91 955 539 5086</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>ADDRESS </b></li>
								<li>101 Sai Veera enclave, Shilpa park Kondapur, Hyderabad.</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>E-MAIL </b></li>
								<li><a href="mailto:example@mail.com"> himant.dewangan@gmail.com</a></li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>WEBSITE </b></li>
								<li><a href="http://himant.co.in:8080/Mysite">www.himant.co.in</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>	
	
	<jsp:include page="footer.jsp"/>