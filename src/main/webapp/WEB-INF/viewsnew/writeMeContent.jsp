<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="content">
	<div class="inner" style="min-height: 700px;">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<header>
						<h5>Messages</h5>
						<div class="toolbar">
							<div class="btn-group">
								<a href="#sortableTable" data-toggle="collapse"
									class="btn btn-default btn-sm accordion-toggle minimize-box">
									<i class="icon-chevron-up"></i>
								</a>
							</div>
						</div>
					</header>
					<div id="sortableTable" class="body in" style="height: auto;">
						<table
							class="table table-bordered sortableTable responsive-table table-hover">
							<thead>
								<tr>
									<!-- <th>#<i class="icon-sort"></i><i class="icon-sort-down"></i>
												<i class="icon-sort-up"></i></th> -->
									<th>Sender<i class="icon-sort"></i><i
										class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
									<th>Message<i class="icon-sort"></i><i
										class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
								</tr>
							</thead>
							<tbody>


								<c:forEach items="${messages}" var="message">
									<tr>
										<td>${message.sender}</td>
										<td>${message.messageText}</td>
									</tr>
								</c:forEach>


							</tbody>
						</table>
						<button class="btn btn-primary btn-md" data-toggle="modal"
							data-target="#myModal">Write me</button>
						<div class="col-lg-12">
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="H4">New Registration</h4>
										</div>
										<div class="modal-body">
											<form:form role="form" modelAttribute="message" action="/Mysite/sendMessage" method="POST">
												<div class="form-group">
													<label>Name</label> <input class="form-control" type="text"
														name="sender" />
													<p class="help-block">Example block-level help text
														here.</p>
												</div>
												<div class="form-group">
													<label>Message</label> <input class="form-control"
														type="text" name="messageText" />
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default"
														data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Save
														changes</button>
												</div>
											</form:form>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>