<jsp:include page="header.jsp" />
<jsp:include page="topHeader.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">
	<div class="row">
		<div class="text-center">
			<h1>Sample Image Gallery</h1>
		</div>
		<div class="form-group">
			<label>Preview File Icon</label> <input name="file-3" id="file-3"
				type="file" multiple class="file-loading" />

			<div id="errorBlock" class="help-block"></div>
		</div>
	</div>
</div>
<div id="kv-success-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Yippee!</h4>
			</div>
			<div id="kv-success-box" class="modal-body"></div>
		</div>
	</div>
</div>

<c:forEach items="${images}" var="image">
<div class="form-group">
		<img src="/Mysite/showImage/${image.imageId}" width="220" height="220" border="0"/>
		</div>
</c:forEach>
<script>
	$("#file-3").fileinput({

		showUpload : true,
		showCaption : true,
		uploadUrl : '/Mysite/uploadImage',
		browseClass : "btn btn-primary btn-lg",
		fileType : "any",

		previewFileIcon : "<i class='glyphicon glyphicon-king'></i>",
		overwriteInitial : false,
		initialPreviewAsData : true,

	}).on('success', function() {
		$('#kv-success-modal').modal('show');
	});

	$(document)
			.ready(
					function() {
						$("#test-upload").fileinput({
							'showPreview' : false,
							'allowedFileExtensions' : [ 'jpg', 'png', 'gif' ],
							'elErrorContainer' : '#errorBlock'
						});
						$("#kv-explorer")
								.fileinput(
										{
											'theme' : 'explorer',
											'uploadUrl' : '#',
											overwriteInitial : false,
											initialPreviewAsData : true,
											initialPreview : [
													"http://lorempixel.com/1920/1080/nature/1",
													"http://lorempixel.com/1920/1080/nature/2",
													"http://lorempixel.com/1920/1080/nature/3", ],
											initialPreviewConfig : [ {
												caption : "nature-1.jpg",
												size : 329892,
												width : "120px",
												url : "{$url}",
												key : 1
											}, {
												caption : "nature-2.jpg",
												size : 872378,
												width : "120px",
												url : "{$url}",
												key : 2
											}, {
												caption : "nature-3.jpg",
												size : 632762,
												width : "120px",
												url : "{$url}",
												key : 3
											}, ]
										});

						/*  $("#file-3").fileinput({
						     uploadUrl: '/Mysite/uploadImage',
						     maxFilePreviewSize: 10240
						 }); */
						/*
						 $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
						 alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
						 });
						 */
					});
</script>