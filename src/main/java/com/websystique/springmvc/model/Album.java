package com.websystique.springmvc.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Arya
 *
 */
@Entity
@Table(name="ALBUM")
public class Album {

	@Id
	@Column(name="ALBUM_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int albumID;

	@NotNull
	@Column(name = "TITLE", nullable = false)
	private String title;

	@Column(name = "COVER_PAGE_URL", nullable = true)
	private String coverPageUrl;

	@Column(name="CREATION_DATE")
	private Date creationDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	public  Album() {
		
	}
	
	public Album(final AlbumVO albumVO) {
		this.title = albumVO.getTitle();
		this.description = albumVO.getDescription();
		this.createdBy = albumVO.getCreatedBy();
		this.creationDate = albumVO.getCreationDate();
		this.coverPageUrl = albumVO.getCoverPageUrl();
	}
	
	public int getAlbumID() {
		return albumID;
	}

	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCoverPageUrl() {
		return coverPageUrl;
	}

	public void setCoverPageUrl(String coverPageUrl) {
		this.coverPageUrl = coverPageUrl;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.albumID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Album))
			return false;
		Album other = (Album) obj;
		if (albumID != other.albumID)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} 
		return true;
	}

	@Override
	public String toString() {
		return "dummy";
	}

	public AlbumVO copyToVO() {
		final AlbumVO result = new AlbumVO();
		result.setAlbumID(this.albumID);
		result.setCoverPageUrl(this.coverPageUrl);
		result.setDescription(this.description);
		result.setCreatedBy(this.createdBy);
		result.setCreationDate(this.creationDate);
		result.setTitle(this.title);
		return result;
	}
	
	
	

}
