package com.websystique.springmvc.model;

import java.sql.Date;
import java.util.List;

/**
 * 
 * @author Arya
 *
 */
public class AlbumVO {

	private int albumID;

	private String title;

	private String coverPageUrl;

	private Date creationDate;
	
	private String createdBy;
	
	private String description;
	
	private List<Image> albumPics;

	public List<Image> getAlbumPics() {
		return albumPics;
	}

	public void setAlbumPics(List<Image> albumPics) {
		this.albumPics = albumPics;
	}

	public int getAlbumID() {
		return albumID;
	}

	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCoverPageUrl() {
		return coverPageUrl;
	}

	public void setCoverPageUrl(String coverPage) {
		this.coverPageUrl = coverPage;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}
