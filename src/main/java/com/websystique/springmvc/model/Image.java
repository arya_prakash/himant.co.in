package com.websystique.springmvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Arya
 *
 */
@Entity
@Table(name="IMAGE")
public class Image {

	@Id
	@Column(name="IMAGE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int imageId;

	@Column(name="ALBUM_ID")
	private int albumId;
	
	/**
	 * The name of the image file, stored as in AWS S3 server.
	 */
	@Column(name="IMAGE_KEY")
	private String imageKey;
	
	
	@Column(name="IMAGE_URL")
	private String imageURL;
	
	
	public int getImageID() {
		return imageId;
	}

	public void setImageID(int imageID) {
		this.imageId = imageID;
	}

	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public String getImageKey() {
		return imageKey;
	}

	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.imageId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Image))
			return false;
		Image other = (Image) obj;
		if (imageId != other.imageId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "dummy";
	}
	
	
	

}
