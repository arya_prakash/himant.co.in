package com.websystique.springmvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalDate;

/**
 * @author Arya
 *
 */
@Entity
@Table(name="MESSAGE")
public class Message {

	@Id
	@Column(name="MESSAGE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int messageID;

	@NotNull
	@Column(name = "SENDER", nullable = false)
	private String sender;

	@NotNull
	@Column(name = "MESSAGE_TEXT", nullable = true)
	private String messageText;

	
	

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.messageID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Message))
			return false;
		Message other = (Message) obj;
		if (messageID != other.messageID)
			return false;
		if (sender == null) {
			if (other.sender != null)
				return false;
		} 
		return true;
	}

	@Override
	public String toString() {
		return "dummy";
	}
	
	
	

}
