package com.websystique.springmvc.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

@Controller
public class LoginController {

	private  GoogleIdTokenVerifier mVerifier;
	private  JsonFactory mJFactory;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String login(@RequestParam("tokenString") String tokenString) {

		mJFactory = new JacksonFactory();
		 try {
			GoogleIdToken token = GoogleIdToken.parse(mJFactory,
			         tokenString);
			
			 GoogleIdToken.Payload payload = token.getPayload();
			 
			 
			 String name = (String) payload.get("name");
			  String pictureUrl = (String) payload.get("picture");
			  String locale = (String) payload.get("locale");
			  String familyName = (String) payload.get("family_name");
			  String givenName = (String) payload.get("given_name");
			  
			  System.out.println("name : " + name);
			  System.out.println("pictureUrl : " + pictureUrl);
			  System.out.println("locale : " + locale);
			  System.out.println("givenName : " + givenName);
			  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "imageGallery";
	}

}