package com.websystique.springmvc.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.websystique.springmvc.model.Image;
import com.websystique.springmvc.service.AlbumService;
import com.websystique.springmvc.service.ImageService;

@Controller
public class ShowImageController {

	@Autowired
	ImageService imageService;

	@Autowired
	AlbumService albumService;

	@RequestMapping(value = "/showImage/{imgId}", method = RequestMethod.GET)
	public void getImage(@PathVariable("imgId") Integer imageId, final HttpServletResponse response, ModelMap model)
			throws IOException, SQLException {
		Image image = imageService.findById(imageId);
		ServletOutputStream out = response.getOutputStream();
		/*InputStream in = image.getContent().getBinaryStream();
		int lenght = 1024;
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		try {
			while ((lenght = in.read(buffer)) != -1) {
				out.write(buffer, 0, lenght);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		in.close();
		out.flush();
*/
	}

}