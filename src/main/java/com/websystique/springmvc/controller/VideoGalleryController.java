package com.websystique.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.websystique.springmvc.service.MessageService;

@Controller
public class VideoGalleryController {
	
	@Autowired
	MessageService messageService;

	  @RequestMapping(value = "/videoGallery", method = RequestMethod.GET)
	    public String getAboutMePage(ModelMap model) {
		  model.addAttribute("activeMenu", "videoGallery");
		  return "videoGallery";
	    }
}
