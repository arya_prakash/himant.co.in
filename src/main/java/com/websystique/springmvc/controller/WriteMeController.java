package com.websystique.springmvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.websystique.springmvc.model.Message;
import com.websystique.springmvc.service.MessageService;

@Controller
public class WriteMeController {
	
	@Autowired
	MessageService messageService;

	  @RequestMapping(value = "/writeme", method = RequestMethod.GET)
	    public String getAboutMePage(ModelMap model) {
	      List<Message> messages = messageService.findAllMessages();
	      model.addAttribute("messages", messages);
		  return "writeMe";
	    }
	  
	  @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
	    public String getMessage(@Valid Message message,
				BindingResult result, ModelMap model) {
		  System.out.println("hello in sendmessage");
		  System.out.println(message.getMessageText());
		  System.out.println(message.getSender());
		  
		  messageService.saveMessage(message);
		  
	      List<Message> messages = messageService.findAllMessages();
	      model.addAttribute("messages", messages);
		  return "writeMe";
	    }
}
