package com.websystique.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MenuController {

	  @RequestMapping(value = "/menu/{menuItem}", method = RequestMethod.GET)
	  @ResponseBody
	    public String getAboutMePage(@PathVariable("menuItem") String menuItem,  ModelMap model) {
		  System.out.println("menuController*************************8");
		  model.addAttribute("activeMenu", menuItem);
	      return menuItem;
	    }
}
