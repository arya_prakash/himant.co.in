package com.websystique.springmvc.controller;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.model.Album;
import com.websystique.springmvc.model.AlbumVO;
import com.websystique.springmvc.service.AlbumService;

@Controller
public class ImageGalleryController {

	@Autowired
	AlbumService albumService;

	@RequestMapping(value = "/imageGallery", method = RequestMethod.GET)
	public String getIndexPage(ModelMap model) {
		List<Album> albums = albumService.findAllAlbums();
		AlbumVO albumVO = new AlbumVO();
		model.addAttribute("albums", albums);
		model.addAttribute("activeMenu", "imageGallery");
		model.addAttribute("albumVO", albumVO);

		return "imageGalleryAlbum";
	}

	@RequestMapping(value = "/createAlbum", method = RequestMethod.POST)
	@ResponseBody
	public String createAlbum(@ModelAttribute("albumVO") AlbumVO albumVO,
			@RequestPart("coverPage") MultipartFile file) {

		System.out.println("with in create album request" + file.getName());
		albumVO.setCreatedBy("Admin");
		albumVO.setCreationDate(new Date(System.currentTimeMillis()));

		albumService.saveAlbum(albumVO, file);
		return "imageGallery";
	}

	@RequestMapping(value = "/gotoAlbum", method = RequestMethod.POST)
	public String exploreAlbum(@RequestParam("albumId") Integer albumId, final HttpServletResponse response, ModelMap model) {
		final AlbumVO albumVO = albumService.findById(albumId);
		model.addAttribute("albumVO", albumVO);
		return "albumPage";
	}
	
	@RequestMapping(value = "/uploadImageToAlbum", method = RequestMethod.POST)
	public String uploadImageToAlbum(@RequestParam("albumId") Integer albumId, @RequestParam("coverPage") MultipartFile file, ModelMap model) {
		
		
		albumService.uploadImageIntoAlbum(file, albumId);
		final AlbumVO albumVO = albumService.findById(albumId);
		System.out.println(albumVO.getAlbumPics().get(0).getImageURL());
		model.addAttribute("albumVO", albumVO);
		return "albumPage";
	}
	
	@RequestMapping(value = "/deleteAlbum", method = RequestMethod.POST)
	public String deleteAlbum(@RequestParam("albumId") Integer albumId, ModelMap model) {
		
		
		System.out.println("deleting album");
		System.out.println("album id : " + albumId) ;
		albumService.removeAlbum(albumId);
		
		List<Album> albums = albumService.findAllAlbums();
		AlbumVO albumVO = new AlbumVO();
		model.addAttribute("albums", albums);
		model.addAttribute("activeMenu", "imageGallery");
		model.addAttribute("albumVO", albumVO);
		return "imageGalleryAlbum";
	}



}