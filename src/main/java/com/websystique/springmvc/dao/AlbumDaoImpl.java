package com.websystique.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.websystique.springmvc.model.Album;
import com.websystique.springmvc.model.AlbumVO;

@Repository("albumDao")
public class AlbumDaoImpl extends AbstractDao<Integer, Album> implements AlbumDao {

	@Override
	public Album findById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveAlbum(AlbumVO albumVO) {
		Album album = new Album(albumVO);
		saveOrUpdate(album);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Album> findAllAlbums() {
		Criteria criteria = createEntityCriteria();
		return (List<Album>) criteria.list();
	}

	@Override
	public void updateAlbum(Album album) {
		persist(album);

	}

	@Override
	public void remoreAlbumByKey(Integer albumId) {
		Album album = findById(albumId);
		delete(album);
		
	}
}
