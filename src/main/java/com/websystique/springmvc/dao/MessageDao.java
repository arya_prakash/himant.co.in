package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.Message;

public interface MessageDao {

	Message findById(final int id);

	void saveMessage(final Message employee);

	List<Message> findAllMessages();
	
	/*void deleteEmployeeBySsn(String ssn);
	
	List<Employee> findAllEmployees();

	Employee findEmployeeBySsn(String ssn);*/

}
