package com.websystique.springmvc.dao;

import java.util.List;

import com.websystique.springmvc.model.Album;
import com.websystique.springmvc.model.AlbumVO;

public interface AlbumDao {

	Album findById(final int id);

	void saveAlbum(final AlbumVO albumVO);

	List<Album> findAllAlbums();
	
	void updateAlbum(final Album album);

	void remoreAlbumByKey(final Integer albumId);

}
