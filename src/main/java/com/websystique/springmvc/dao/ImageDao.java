package com.websystique.springmvc.dao;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.model.Image;

public interface ImageDao {

	Image findById(final int id);

	void saveImage(final Image image);

	List<Image> findAllImages();

	void saveImage(MultipartFile file);

	List<Image> findAllImagesByAlbumID(final int albumID);

	void removeImagesByAlbumId(final Integer albumId);
	

}
