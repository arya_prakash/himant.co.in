package com.websystique.springmvc.dao;

import java.io.IOException;
import java.sql.Blob;
import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.model.Image;
import com.websystique.springmvc.model.Message;

@Repository("imageDao")
public class ImageDaoImpl extends AbstractDao<Integer, Image> implements ImageDao {

	public void deleteEmployeeBySsn(String ssn) {
		Query query = getSession().createSQLQuery("delete from Employee where ssn = :ssn");
		query.setString("ssn", ssn);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Message> findAllEmployees() {
		Criteria criteria = createEntityCriteria();
		return (List<Message>) criteria.list();
	}

	public Message findEmployeeBySsn(String ssn) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("ssn", ssn));
		return (Message) criteria.uniqueResult();
	}


	@SuppressWarnings("unchecked")
	public List<Message> findAllMessages() {
		Criteria criteria = createEntityCriteria();
		return (List<Message>) criteria.list();
	}

	@Override
	public Image findById(int id) {
		return getByKey(id);
	}

	@Override
	public void saveImage(Image image) {
		saveOrUpdate(image);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Image> findAllImages() {
		Criteria criteria = createEntityCriteria();
		return (List<Image>) criteria.list();
	}

	@Override
	public void saveImage(MultipartFile file) {
		try {
			Blob blob = Hibernate.getLobCreator(getSession()).createBlob(file.getBytes());
			Image image = new Image();
			saveImage(image);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Image> findAllImagesByAlbumID(int albumID) {
		List<Image> images = createEntityCriteria().add(Restrictions.eq("albumId", albumID)).list();
		return images.isEmpty() ? Collections.EMPTY_LIST : images;
	}

	@Override
	public void removeImagesByAlbumId(Integer albumId) {
		Query query = getSession().createSQLQuery("delete from IMAGE where ALBUM_ID = :albumId");
		query.setInteger("albumId", albumId);
		query.executeUpdate();
		
	}
}
