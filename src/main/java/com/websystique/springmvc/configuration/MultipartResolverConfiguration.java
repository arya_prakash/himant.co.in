package com.websystique.springmvc.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.websystique.springmvc")
public class MultipartResolverConfiguration {
	
	  @Bean
	    public CommonsMultipartResolver multiPartResolver(){

		  System.out.println("called here...");
	        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
	        return resolver;
	    }
	  
	  @Bean
	  @Order(0)
	  public MultipartFilter multipartFilter() {
	      return new MultipartFilter();
	  }
}