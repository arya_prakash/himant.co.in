package com.websystique.springmvc.service;

import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.himant.co.in.constants.AWSConstantPool;
import com.websystique.springmvc.dao.AlbumDao;
import com.websystique.springmvc.model.Album;
import com.websystique.springmvc.model.AlbumVO;
import com.websystique.springmvc.model.Image;

@Service("albumService")
@Transactional
public class AlbumServiceImpl implements AlbumService {

	@Autowired
	private AlbumDao albumDao;
	
	@Autowired
	private AwsS3Service awsS3Service;
	
	@Autowired
	private ImageService imageService;
	
	final static String ALLOWED_CHARSET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	
	@Override
	public AlbumVO findById(int id) {
		Album album = albumDao.findById(id);
		AlbumVO albumVO = album.copyToVO();
		albumVO.setAlbumPics(getAllImagesOfAlbum(albumVO.getAlbumID()));
		return albumVO;
		
	}

	@Override
	public void saveAlbum(AlbumVO albumVO, final MultipartFile file) {
		final String fileName = System.currentTimeMillis() + "_" + RandomStringUtils.random(10, ALLOWED_CHARSET) + AWSConstantPool.FILE_TYPE_SUFFIX.getValue();
		
		awsS3Service.uploadFile(file, fileName );
		albumVO.setCoverPageUrl(AWSConstantPool.SERVER_END_POINT.getValue() + fileName);
		albumDao.saveAlbum(albumVO);
		
	}

	@Override
	public void updateAlbum(Album album) {
		albumDao.updateAlbum(album);
		
	}

	@Override
	public List<Album> findAllAlbums() {
		return albumDao.findAllAlbums();
	}
	
	private List<Image> getAllImagesOfAlbum(int albumID) {
		List<Image> images = imageService.findAllImageByAlbumId(albumID);
		return images;
	}

	@Override
	public void uploadImageIntoAlbum(MultipartFile imageFile, Integer albumId) {
		final String imageFileName = System.currentTimeMillis() + "_" + RandomStringUtils.random(10, ALLOWED_CHARSET) + AWSConstantPool.FILE_TYPE_SUFFIX.getValue();
		
		awsS3Service.uploadFile(imageFile, imageFileName );
		Image image = new Image();
		image.setAlbumId(albumId);
		image.setImageKey(imageFileName);
		image.setImageURL(AWSConstantPool.SERVER_END_POINT.getValue() + imageFileName);
		
		imageService.saveImage(image);
		
	}

	@Override
	public void removeAlbum(final Integer albumId) {
		try {
			imageService.removeImagesByAlbumId(albumId);	
			albumDao.remoreAlbumByKey(albumId);
		}catch (Exception e) {
			
		}
		
	}

}
