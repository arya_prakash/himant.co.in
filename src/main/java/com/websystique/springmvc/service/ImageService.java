package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.model.Image;

public interface ImageService {

	Image findById(int id);
	
	void saveImage(Image image);
	
	void saveImage(MultipartFile file);
	
	List<Image> findAllImages();

	List<Image> findAllImageByAlbumId(int albumID);

	void removeImagesByAlbumId(final Integer albumId); 

	
}
