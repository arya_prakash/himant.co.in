package com.websystique.springmvc.service;

import org.springframework.web.multipart.MultipartFile;

public interface AwsS3Service {

    public void uploadFile(MultipartFile uploadFile, String fileName);
	
}