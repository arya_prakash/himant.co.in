package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.dao.ImageDao;
import com.websystique.springmvc.model.Image;

@Service("imageService")
@Transactional
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageDao imageDao;
	

	@Override
	public Image findById(int id) {
		return imageDao.findById(id);
	}

	@Override
	public void saveImage(Image image) {
		imageDao.saveImage(image);
		
	}

	@Override
	public List<Image> findAllImages() {
		return imageDao.findAllImages();
	}

	@Override
	public void saveImage(MultipartFile file) {
		imageDao.saveImage(file);
		
	}

	@Override
	public List<Image> findAllImageByAlbumId(int albumID) {
		return imageDao.findAllImagesByAlbumID(albumID);
	}

	@Override
	public void removeImagesByAlbumId(Integer albumId) {
		imageDao.removeImagesByAlbumId(albumId);
		
	}

}
