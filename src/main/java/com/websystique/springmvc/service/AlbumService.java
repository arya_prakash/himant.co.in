package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.websystique.springmvc.model.Album;
import com.websystique.springmvc.model.AlbumVO;

public interface AlbumService {

	AlbumVO findById(final int id);
	
	void saveAlbum(final AlbumVO albumVO, final MultipartFile file);
	
	void updateAlbum(final Album album);
	
	List<Album> findAllAlbums();
	
	void uploadImageIntoAlbum(final MultipartFile imageFile, final Integer albumId);

	void removeAlbum(final Integer albumId);

}
