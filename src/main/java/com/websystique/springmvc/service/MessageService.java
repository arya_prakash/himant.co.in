package com.websystique.springmvc.service;

import java.util.List;

import com.websystique.springmvc.model.Message;

public interface MessageService {

	Message findById(int id);
	
	void saveMessage(Message Message);
	
	void updateMessage(Message Message);
	
	List<Message> findAllMessages(); 
	/*void deleteMessageBySsn(String ssn);

	
	Message findMessageBySsn(String ssn);

	boolean isMessageSsnUnique(Integer id, String ssn);*/
	
}
