package com.websystique.springmvc.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service("awsS3Service")
public class AwsS3ServiceImpl implements AwsS3Service {

	@Autowired
	private AmazonS3 s3client;

	@Value("${aws_namecard_bucket}")
	private String nameCardBucket;

	@Override
	public void uploadFile(MultipartFile uploadFile, String fileName) {
		InputStream stream;
		try {
			stream = uploadFile.getInputStream();
			s3client.setEndpoint("http://s3.amazonaws.com");

			s3client.putObject(new PutObjectRequest(nameCardBucket, fileName, stream, new ObjectMetadata())
					.withCannedAcl(CannedAccessControlList.PublicRead));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
