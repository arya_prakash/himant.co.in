package com.websystique.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.websystique.springmvc.dao.MessageDao;
import com.websystique.springmvc.model.Message;

@Service("messageService")
@Transactional
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageDao messageDao;
	
	public Message findById(int id) {
		return messageDao.findById(id);
	}

	public void saveMessage(Message Message) {
		messageDao.saveMessage(Message);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateMessage(Message message) {
		Message entity = messageDao.findById(message.getMessageID());
		if(entity!=null){
			entity.setSender(message.getSender());
			entity.setMessageText(message.getMessageText());
		}
	}

	/*public void deleteMessageBySsn(String ssn) {
		dao.deleteMessageBySsn(ssn);
	}*/
	
	public List<Message> findAllMessages() {
		return messageDao.findAllMessages();
	}

	/*
	public Message findMessageBySsn(String ssn) {
		return dao.findMessageBySsn(ssn);
	}

	public boolean isMessageSsnUnique(Integer id, String ssn) {
		Message Message = findMessageBySsn(ssn);
		return ( Message == null || ((id != null) && (Message.getId() == id)));
	}*/
	
}
