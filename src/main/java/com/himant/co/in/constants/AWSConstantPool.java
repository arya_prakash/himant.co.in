package com.himant.co.in.constants;

public enum AWSConstantPool {
	
	FILE_TYPE_SUFFIX(".jpg"),
	SERVER_END_POINT("https://s3.ap-south-1.amazonaws.com/himant-image/");
	
	private AWSConstantPool(final String value) {
		this.value = value;
	}
	
	
	private String value;


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	
	
}
